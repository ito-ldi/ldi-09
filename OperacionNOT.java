/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lesm.operaciones;

/**
 *
 * @author Luis
 */
public class OperacionNOT {
    private String numeroBinario;
    
    public OperacionNOT(String numero){
        this.numeroBinario = numero;
    }
    
    public String operacionNOT(String numero){
          return invertirValores(numero);
    }
   private String invertirValores(String numero){   
        String[] arre = new String[numero.length()];
        int pos = 0;
        String resul = "";
        for(int x = numero.length(); x >0; x--){
            String digito = numero.substring(x-1, x);
            if(digito != null){
                if(digito.equals("1")){
                 arre[pos] = "0";
                }else{arre[pos] = "1";}
                pos++;
            }            
        }
         for(int x = arre.length-1; x >= 0; x--){            
                resul += arre[x];
        }
         return resul;
    }
     public String getNumeroBinario() {
        return numeroBinario;
    }
    public void setNumeroBinario(String numeroBinario) {
        this.numeroBinario = numeroBinario;
    }
     public static void main(String[] args){
        OperacionNOT operaciones = new OperacionNOT("11011101011010");
        String negacion = operaciones.operacionNOT(operaciones.getNumeroBinario());
        System.out.println("valor de entrada = "+operaciones.getNumeroBinario()
                          +"\nsu negacion es   = "+negacion);
    }
      
}
