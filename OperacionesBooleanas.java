/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lesm.operaciones;

/**
 *
 * @author Luis
 */
public class OperacionesBooleanas{
    private String numBinarioUno;
    private String numBinarioDos;
    private String numBinarioTres;
    
    public OperacionesBooleanas(String numUno, String numDos){
        this(numUno,numDos,"");
    }
    public OperacionesBooleanas(String numUno, String numDos, String numTres){
        this.numBinarioUno = numUno;
        this.numBinarioDos = numDos;
        this.numBinarioTres = numTres;
    }
    public String funcionUno(String numA,String numB){
         System.out.println("Función número uno");
         OperacionNOT  not = new OperacionNOT(numA);
         String negacion = not.operacionNOT(not.getNumeroBinario());
         System.out.println("Negacion de numA "+getNumBinarioUno()+" = "+negacion);
         String resultado = sumaDeBinarios(negacion, numB);
         System.out.println("Suma de "
                           +"\n        "+negacion
                           +"\n     +  "+getNumBinarioDos());
         
         return resultado;
    }
    
    public String funcionDos(String numA, String numB){
         OperacionNOT  not = new OperacionNOT(numB);
         String negacion = not.operacionNOT(not.getNumeroBinario());
         OperacionAND and = new OperacionAND(numA,negacion);
         String resultado = and.realizaOperacionAND(and.getNumBinarioUno(),and.getNumBinarioDos());
         
        return resultado;
    }
    public String funcionTres(String numA, String numB, String numS){
         OperacionAND and = new OperacionAND(numB,numS);
         String resAND = and.realizaOperacionAND(and.getNumBinarioUno(),and.getNumBinarioDos());
         
         OperacionNOT  not = new OperacionNOT(numS);
         String resNOT = not.operacionNOT(not.getNumeroBinario());
         
         OperacionAND and2 = new OperacionAND(numA, resNOT);
         String resAND2 = and2.realizaOperacionAND(and2.getNumBinarioUno(), and2.getNumBinarioDos());
         
           String resultado = sumaDeBinarios(resAND, resAND2);
         
         
        return resultado;
    }
    
     
     public String sumaDeBinarios(String numBinario1, String numBinario2){
        String[] arr;
        arr = (numBinario1.length() >= numBinario2.length()) ? new String[numBinario1.length()+1]: new String[numBinario2.length()+1];
        int n = 0;       
        String resultado = "";
        String acarreo = "0";
        String num1 = "";
        String num2 = "";
         int longitudUno = numBinario1.length();
         int longitudDos = numBinario2.length();
        while(longitudUno >= 0 || longitudDos >= 0){         
            if(longitudUno > 0){
                try{
                  num1 = numBinario1.substring((longitudUno-1),longitudUno);                 
                }catch(Exception e){}                 
            }else{num1 = "0"; }
            longitudUno--;
            if(longitudDos >0){
                try{
                num2 = numBinario2.substring((longitudDos-1), longitudDos);
                }catch(Exception ee){}                 
            }else{num2 = "0"; } 
            longitudDos--;
            if(num1.equals(num2) && num1.equals("1")){                             
                if(acarreo.equals("1")){                  
                   arr[n] = "1";
                   acarreo = "1";
                }else{arr[n] = "0"; acarreo = "1";}    
            }else if(num1.equals(num2) && num1.equals("0")){               
                if(acarreo.equals("1")){                   
                   arr[n]="1";
                   acarreo = "0";
                }else{arr[n]="0";}                
            }else{                 
                if(acarreo.equals("1")){                  
                    arr[n]="0";
                   acarreo = "1";
                }else{arr[n]="1";}                
             }           
            n++;
        }             
        for(int x = arr.length-1; x >= 0; x--){            
                resultado += arr[x];
        }
        return resultado.substring(getNumBinarioUno().length()-8,getNumBinarioUno().length());
    }
    
    public String getNumBinarioUno() {
        return numBinarioUno;
    }
    public void setNumBinarioUno(String numBinarioUno) {
        this.numBinarioUno = numBinarioUno;
    }
    public String getNumBinarioDos() {
        return numBinarioDos;
    }
    public void setNumBinarioDos(String numBinarioDos) {
        this.numBinarioDos = numBinarioDos;
    }
    public String getNumBinarioTres() {
        return numBinarioTres;
    }
    public void setNumBinarioTres(String numBinarioTres) {
        this.numBinarioTres = numBinarioTres;
    }
   
   public static void main(String[] args){
       OperacionesBooleanas operacion = new OperacionesBooleanas("11111011","11101011","11101111");
       
       String funcionUno = operacion.funcionUno(operacion.getNumBinarioUno(),operacion.getNumBinarioDos());
       String funcionDos = operacion.funcionDos(operacion.getNumBinarioUno(),operacion.getNumBinarioDos());
       String funcionTres = operacion.funcionTres(operacion.getNumBinarioUno(),operacion.getNumBinarioDos(),operacion.getNumBinarioTres());
       
       
       System.out.println("\nResultado de la función uno: "+ funcionUno);
       System.out.println("\nResultado de la función dos: "+ funcionDos);
       System.out.println("\nResultado de la función tres "+ funcionTres);
                         
   }
    
   
}
