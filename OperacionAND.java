/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lesm.operaciones;

/**
 *
 * @author Luis
 */
public class OperacionAND{
    private String numBinarioUno;
    private String numBinarioDos;
    
    public OperacionAND(String numBinarioUno, String numBinarioDos){
        this.numBinarioUno = numBinarioUno;
        this.numBinarioDos = numBinarioDos;
    }
    
    public String realizaOperacionAND(String numeUno, String numDos){
        String resul = "";
        try{
            String[] arre = new String[numeUno.length()];
        int pos = 0;
        if(numeUno.length() == numDos.length()){
            for(int x = numeUno.length(); x >0; x--){
            String digitoDeNumeroUno = numeUno.substring(x-1, x);
            String digitoDeNumeroDos = numDos.substring(x-1, x);
            if(digitoDeNumeroUno != null && digitoDeNumeroDos != null){
                if(digitoDeNumeroUno.equals(digitoDeNumeroDos) && digitoDeNumeroUno.equals("1")){
                 arre[pos] = "1";
                }else{arre[pos] = "0";}
                pos++;
            }            
        }
         for(int x = arre.length-1; x >= 0; x--){            
                resul += arre[x];
        }
        }else{System.out.println("El número de digitos debe de ser iguales\n Para poder realizar lo operación\n");}
        
        }catch(Exception e){
            System.out.println("Los números deben de ser de la misma longitud, \npara poder hacer la operación AND");
        }
         return resul;
    }

    public String getNumBinarioUno() {
        return numBinarioUno;
    }
    public void setNumBinarioUno(String numBinarioUno) {
        this.numBinarioUno = numBinarioUno;
    }
    public String getNumBinarioDos() {
        return numBinarioDos;
    }
    public void setNumBinarioDos(String numBinarioDos) {
        this.numBinarioDos = numBinarioDos;
    }
    
    public static void main(String[] args){
        OperacionAND operacion = new OperacionAND("01010111", "10010101");
        String and = operacion.realizaOperacionAND(operacion.getNumBinarioUno(),operacion.getNumBinarioDos());
        System.out.println("La operación AND de "
                          +"\n\t\t    "+operacion.getNumBinarioUno()
                          +"\n\t\t    "+operacion.getNumBinarioDos()
                          +"\n\nel resultado es   = "+and);
    }
}
